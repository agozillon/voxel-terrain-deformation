#pragma once

#include <GL\glew.h>
#include <memory>

#include "../Texture/Texture2D.h"

// Texture Loading class that uses the SDL_Image 2.0 plugin to load in textures and pass them to the 
// GPU, Should support BMP, GIF, JPEG, LBM, PCX, PNG, PNM, TGA, TIFF, WEBP, XCF, XPM, XV. Have tested
// PNG, BMP and JPEG. Similar to my previously used texture loader except that it's modified to support multiple texture types + Texture2D 
// class support.
class TextureLoader
{

public:
	// pass in the file path of the texture and it'll create and return a Texture2D object that's a shared pointer
	// the Texture2D object will have the usual default wrap/filtering parameters applied to it that are normal to
	// diffuse textures.
	std::shared_ptr<Texture2D> loadTexture(char *fname);

private:

};
