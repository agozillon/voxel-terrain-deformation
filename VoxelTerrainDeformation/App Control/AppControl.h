#pragma once

#include <glm/gtc/type_ptr.hpp>
#include <SDL_ttf.h>   // including SDL_ttf to set up the textFont
#include <SDL.h>	   // including SDL to set up the SDL window
#include <memory>

#include "AppState.h" // including GameState so that I can create pointers and functions for the State objects


// This class is an singleton it contains the main run time loop. Sets up the GL context, instantiates the SDL window, cleans it up and swaps buffers.
// Also contains all major states and manages them by allowing you to switch from state to state via functions (it's a singleton so 
// can be used anywhere). USED IN GED/AGP/RT3D JUST MODIFIED SLIGHTLY, ANYTHING NEW WILL CONTAIN KEYWORD UNUSED IN COMMENT
class AppControl
{

public:
	static AppControl* getInstance();                                      // function that returns a pointer to a AppControl Object so we can use the functions from here(static so that its useable from anywhere without an instansiated AppControl object)
	void run();                                                            // function that contains the game run loop 
	void setState(AppState* newState);                                     // function to change the current state to the passed in AppState 
	AppState* getState(){ return m_currentState; }						   // inline function returns the currentState AppState pointer 
	AppState* getDemoState(){ return m_demoState; }						   // inline function returns the demoState AppState pointer
	SDL_Window* getSDLWindow(){ return m_hWindow; }                        // inline function that returns the created SDL window 
	glm::vec2 getWindowSize(){ return m_windowSize; }					   // inline function that returns the window size
	float getAspectRatio(){ return m_aspectRatio; }                        // inline function that returns the aspect ratio of the screen
	~AppControl();                                                         // AppControl destructor 

private:
	AppControl();                                                          // private AppControl constructor so that no other class can accidently create an AppControl object (Singleton so there should only ever be one!) 
	AppControl(AppControl const&){};                                       // creating my own copy constructor that is blank and private so that no copies can be created for my singleton
	SDL_Window* setupRC(SDL_GLContext &context);                           // creates an OpenGL context for use with SDL and also creates an SDL window
	
	TTF_Font* m_textFont;												   // TTF_Font used to initalize and check that SDL_ttf is working on initilization
	
	static AppControl* m_instance;                                         // static AppControl pointer (for the singleton so there is only one of these)
	SDL_GLContext m_glContext;										       // OpenGL context handle 
	SDL_Window* m_hWindow;                                                 // SDL_Window type to hold the SDL_Window return from setupRC
      
	// AppState pointer for holding the various states 
	AppState* m_currentState;                                              
	AppState* m_demoState;
	
	bool m_running;                                                          
	glm::vec2 m_windowSize;
	float m_aspectRatio; 
};
