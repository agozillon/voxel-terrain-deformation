#include "ShaderManager.h"

#include <iostream>

ShaderManager::ShaderManager()
{
	// null program that gets returned if you done goofed
#ifdef _DEBUG
	m_nullProgram = std::shared_ptr<GLSLShaderProgram>(new GLSLShaderProgram());
#endif
}

// Instantiates a new GLSLShader which automatically compiles a shader from the pass in file name and type!
// the shader code in the file and the Shadertype must be the same otherwise it'll flag up shader errors
void ShaderManager::compileShader(const char* shaderMapName, const char* fileName, ShaderType type)
{
	m_shaderList[shaderMapName] = std::unique_ptr<GLSLShader>(new GLSLShader(fileName, type));

	// if in debug print shader errors
#ifdef _DEBUG
	printError(m_shaderList[shaderMapName]->getShaderID());
#endif
}


// Adds an attribute to the vertex shader of the program I.E it's inputs so equivelant to stating
// layout (location = 0) in_Position etc. in the shader!
void ShaderManager::addShaderProgramAttribute(const char* programName, const int index, const char* attribName)
{
// simple error check function, checks if the shader progams actually in the list before calling 
// a functin on it. It's only an error check because when called correctly its guaranteed to be in the list.
// The Preproccessor if essentially checks if its in debug if its in debug it'll do the error check!
// If its in release it won't even compile the code saving some CPU cycles.
#ifdef _DEBUG
	if (m_shaderProgramList.find(programName) == m_shaderProgramList.end())
	{
		std::cout << "ShaderProgram is non-existant, perhaps you've yet to create it!" << std::endl;
		return;
	}
#endif

	m_shaderProgramList[programName]->addAttribute(index, attribName);
}

// Adds an output to the fragment shader of the program with the specified index as the location 
// so similar to adding an attribute 
void ShaderManager::addShaderFragOutput(const char* programName, const int index, const char* fragOutputName)
{
#ifdef _DEBUG
	if (m_shaderProgramList.find(programName) == m_shaderProgramList.end())
	{
		std::cout << "ShaderProgram is non-existant, perhaps you've yet to create it!" << std::endl;
		return;
	}
#endif

	m_shaderProgramList[programName]->addFragOutput(index, fragOutputName);
}

// Handy function that basically just sets up basic phong shading attributes and outputs for the shader
void ShaderManager::defaultProgramAttributesAndOutputs(const char* programName)
{
#ifdef _DEBUG
	if (m_shaderProgramList.find(programName) == m_shaderProgramList.end())
	{
		std::cout << "ShaderProgram is non-existant, perhaps you've yet to create it!" << std::endl;
		return;
	}
#endif

	m_shaderProgramList[programName]->addDefaultAttributes();
	m_shaderProgramList[programName]->addDefaultOutput();
}

// Attaches a shader to the program, I.E attaching a geometry shader, vertex shader and frag shader
// all together to make a program! Or other sections of a shader
void ShaderManager::attachShaderToProgram(const char* programName, const char* shaderName)
{
#ifdef _DEBUG
	if (m_shaderProgramList.find(programName) == m_shaderProgramList.end())
	{
		std::cout << "ShaderProgram is non-existant, perhaps you've yet to create it!" << std::endl;
		return;
	}

	if (m_shaderList.find(shaderName) == m_shaderList.end())
	{
		std::cout << "Shader is non existant, perhaps its yet to be loaded and compiled!" << std::endl;
		return;
	}
#endif

	m_shaderProgramList[programName]->attachShader(m_shaderList[shaderName]->getShaderID());
}

// Instantiates a new ShaderProgram and adds it to the map with the specified name, must be done before
// you call anything on the specified shader!
void ShaderManager::createShaderProgram(const char* programName)
{
	m_shaderProgramList[programName] = std::shared_ptr<GLSLShaderProgram>(new GLSLShaderProgram());
}

// Compiles the specified shader program on the GPU linking it etc. and allowing it to be useable
void ShaderManager::compileShaderProgram(const char* programName)
{
#ifdef _DEBUG
	if (m_shaderProgramList.find(programName) == m_shaderProgramList.end())
	{
		std::cout << "ShaderProgram is non-existant, perhaps you've yet to create it!" << std::endl;
		return;
	}
#endif

	m_shaderProgramList[programName]->compileShaderProgram();

	// prints shader program errors when in debug mode
#ifdef _DEBUG
	printError(m_shaderProgramList[programName]->getShaderID());
#endif
}

// returns the shader specified by the passed in char string
std::shared_ptr<GLSLShaderProgram> ShaderManager::getShaderProgram(const char* programName)
{
#ifdef _DEBUG
	if (m_shaderProgramList.find(programName) == m_shaderProgramList.end())
	{
		std::cout << "ShaderProgram is non-existant, perhaps you've yet to create it!" << std::endl;
		return m_nullProgram;
	}
#endif

	return m_shaderProgramList[programName];
}

// Manually deletes all the shaders(NOT PROGRAMS) so should be called after all the programs
// have been created to clean up memory, doesn't delete the shaders on the GPU until the programs
// have been deleted or they've been detached 
void ShaderManager::deleteShaders()
{
	// manually deleting the held onto pointers
	for (std::map<std::string, std::unique_ptr<GLSLShader>>::iterator it = m_shaderList.begin(); it != m_shaderList.end(); ++it)
		it->second.reset();

	m_shaderList.clear();
}

// prints all the GLSL shader errors that have occurred 
void ShaderManager::printError(GLuint id) const
{
	int maxLength = 0;
	int logLength = 0;
	GLchar *logMessage;

	// Find out how long the error message is
	if (!glIsShader(id))
		glGetProgramiv(id, GL_INFO_LOG_LENGTH, &maxLength);
	else
		glGetShaderiv(id, GL_INFO_LOG_LENGTH, &maxLength);

	if (maxLength > 1) { // If message has some contents
		logMessage = new GLchar[maxLength];
		if (!glIsShader(id))
			glGetProgramInfoLog(id, maxLength, &logLength, logMessage);
		else
			glGetShaderInfoLog(id, maxLength, &logLength, logMessage);
		std::cout << "Shader Info Log:" << std::endl << logMessage << std::endl;
		delete[] logMessage;
	}
}