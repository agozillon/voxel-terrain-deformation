#pragma once
#include "Node.h"
#include <memory>
#include <vector>

class Octree {
public:
	Octree(int dimensionOfData, std::vector<std::vector<std::vector<std::vector<int>>>> densityField);
	~Octree(){}

private:
	bool checkSignChange(std::vector<int> signData);

	std::shared_ptr<Node> createExternalNode();
	std::shared_ptr<Node> createInternalNode(std::shared_ptr<Node> currentNode);
	
	std::shared_ptr<Node> m_rootNode;
};