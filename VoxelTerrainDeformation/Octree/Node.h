#pragma once
#include <memory>
#include <glm/gtc/type_ptr.hpp>

enum NodeType {
	EXTERNAL = 0,
	INTERNAL = 1
};

// Abstract Node class that all nodes inherit from!
class Node{
public:
	~Node(){}
	virtual std::shared_ptr<Node> * getChildren(){ return nullptr; }
	virtual glm::vec3 getPos() = 0;
	virtual glm::vec3 getDimensions() = 0;

private:
	
};