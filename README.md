**Instructions**

The project is a visual studio 2013 program and thus requires visual studio 2013 to run. The dev folder can be download from here if you haven’t already got it: https://www.dropbox.com/s/ynoziipbmhaeu10/dev.zip?dl=0
 it should be placed within your C:/ drive at which point you should be able to run the project without any issues. 

**Controls for the Demo**

Keys
Enter:	Regenerates the Voxel world with the values currently displayed on the sliders.
W, S, A, D, R & F:	Moves camera forward , back, left, right, up and down respectively.
Left Arrow, Right Arrow, Up Arrow and Down Arrow: Rotate camera left, right, up and down.

Graphic User Interface How to
Sliders: Hold the mouse button and drag the red piece of the slider to your desired location to change the value.