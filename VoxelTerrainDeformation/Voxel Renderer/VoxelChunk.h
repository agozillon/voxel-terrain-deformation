#pragma once
#include <memory>

#include "../Camera/CameraManager.h"
#include "../Shader Manager/ShaderManager.h"
#include "Voxel.h"
#include "VoxelMerger.h"

class VoxelChunk
{
public:
	VoxelChunk(std::shared_ptr<VoxelMerger> voxelMerger, int worldXPlacement, int worldYPlacement, int worldZPlacement, std::vector<std::vector<float>>& noiseValues);
	~VoxelChunk(){}
	void render(std::shared_ptr<CameraManager> cameraManager, std::shared_ptr<ShaderManager> shaderManager);
	static int getChunkSize(){ return m_chunkSize; }

private:
	static int m_chunkSize;
	std::shared_ptr<Voxel> m_voxels[16][16][16]; // x, y, z list of voxels

	GLuint m_meshVAO;
	GLuint m_chunkIndexCount;
};