#include "VoxelMerger.h"



void VoxelMerger::addVoxel(glm::vec3 position, float blockSize)
{
	// Calculate 8 points of the cube, point1 = front left bottom, point2 = front right bottom, 
	// point 3 = front right top, point 4 =  front left top, point 5 = back right bottom
	// point 6 = back left bottom, point 7 = back left top, point 8 = back right top
	glm::vec3 point1(position.x - blockSize, position.y - blockSize, position.z + blockSize);
	glm::vec3 point2(position.x + blockSize, position.y - blockSize, position.z + blockSize);
	glm::vec3 point3(position.x + blockSize, position.y + blockSize, position.z + blockSize);
	glm::vec3 point4(position.x - blockSize, position.y + blockSize, position.z + blockSize);
	glm::vec3 point5(position.x + blockSize, position.y - blockSize, position.z - blockSize);
	glm::vec3 point6(position.x - blockSize, position.y - blockSize, position.z - blockSize);
	glm::vec3 point7(position.x - blockSize, position.y + blockSize, position.z - blockSize);
	glm::vec3 point8(position.x + blockSize, position.y + blockSize, position.z - blockSize);

	
	glm::vec2 uv1(0.0f, 1.0f);
	glm::vec2 uv2(0.0f, 0.0f);
	glm::vec2 uv3(1.0f, 1.0f);
	glm::vec2 uv4(1.0f, 0.0f);

	// front
	m_mergedMesh.push_back(point1.x);
	m_mergedMesh.push_back(point1.y);
	m_mergedMesh.push_back(point1.z);
	m_mergedMesh.push_back(point3.x);
	m_mergedMesh.push_back(point3.y);
	m_mergedMesh.push_back(point3.z);
	m_mergedMesh.push_back(point4.x);
	m_mergedMesh.push_back(point4.y);
	m_mergedMesh.push_back(point4.z);

	
	m_mergedUV.push_back(uv2.x);
	m_mergedUV.push_back(uv2.y);
	m_mergedUV.push_back(uv3.x);
	m_mergedUV.push_back(uv3.y);
	m_mergedUV.push_back(uv1.x);
	m_mergedUV.push_back(uv1.y);


	// front 
	m_mergedMesh.push_back(point1.x);
	m_mergedMesh.push_back(point1.y);
	m_mergedMesh.push_back(point1.z);
	m_mergedMesh.push_back(point2.x);
	m_mergedMesh.push_back(point2.y);
	m_mergedMesh.push_back(point2.z);
	m_mergedMesh.push_back(point3.x);
	m_mergedMesh.push_back(point3.y);
	m_mergedMesh.push_back(point3.z);

	
	m_mergedUV.push_back(uv2.x);
	m_mergedUV.push_back(uv2.y);
	m_mergedUV.push_back(uv4.x);
	m_mergedUV.push_back(uv4.y);
	m_mergedUV.push_back(uv3.x);
	m_mergedUV.push_back(uv3.y);
	

	// back
	m_mergedMesh.push_back(point5.x);
	m_mergedMesh.push_back(point5.y);
	m_mergedMesh.push_back(point5.z);
	m_mergedMesh.push_back(point7.x);
	m_mergedMesh.push_back(point7.y);
	m_mergedMesh.push_back(point7.z);
	m_mergedMesh.push_back(point8.x);
	m_mergedMesh.push_back(point8.y);
	m_mergedMesh.push_back(point8.z);

	// p5 uv2, p7 uv3, p6 uv4, p8 uv1 
	m_mergedUV.push_back(uv2.x);
	m_mergedUV.push_back(uv2.y);
	m_mergedUV.push_back(uv3.x);
	m_mergedUV.push_back(uv3.y);
	m_mergedUV.push_back(uv1.x);
	m_mergedUV.push_back(uv1.y);
	
	// back
	m_mergedMesh.push_back(point5.x);
	m_mergedMesh.push_back(point5.y);
	m_mergedMesh.push_back(point5.z);
	m_mergedMesh.push_back(point6.x);
	m_mergedMesh.push_back(point6.y);
	m_mergedMesh.push_back(point6.z);
	m_mergedMesh.push_back(point7.x);
	m_mergedMesh.push_back(point7.y);
	m_mergedMesh.push_back(point7.z);

	// p5 uv2, p7 uv3, p6 uv4, p8 uv1 
	m_mergedUV.push_back(uv2.x);
	m_mergedUV.push_back(uv2.y);
	m_mergedUV.push_back(uv4.x);
	m_mergedUV.push_back(uv4.y);
	m_mergedUV.push_back(uv3.x);
	m_mergedUV.push_back(uv3.y);
	

	// right
	m_mergedMesh.push_back(point2.x);
	m_mergedMesh.push_back(point2.y);
	m_mergedMesh.push_back(point2.z);
	m_mergedMesh.push_back(point8.x);
	m_mergedMesh.push_back(point8.y);
	m_mergedMesh.push_back(point8.z);
	m_mergedMesh.push_back(point3.x);
	m_mergedMesh.push_back(point3.y);
	m_mergedMesh.push_back(point3.z);

	// p2 uv4, p8 uv1, p3 uv3, p5 uv2
	m_mergedUV.push_back(uv4.x);
	m_mergedUV.push_back(uv4.y);
	m_mergedUV.push_back(uv1.x);
	m_mergedUV.push_back(uv1.y);
	m_mergedUV.push_back(uv3.x);
	m_mergedUV.push_back(uv3.y);
	

	// right
	m_mergedMesh.push_back(point2.x);
	m_mergedMesh.push_back(point2.y);
	m_mergedMesh.push_back(point2.z);
	m_mergedMesh.push_back(point5.x);
	m_mergedMesh.push_back(point5.y);
	m_mergedMesh.push_back(point5.z);
	m_mergedMesh.push_back(point8.x);
	m_mergedMesh.push_back(point8.y);
	m_mergedMesh.push_back(point8.z);

	// p2 uv4, p8 uv1, p3 uv3, p5 uv2
	m_mergedUV.push_back(uv4.x);
	m_mergedUV.push_back(uv4.y);
	m_mergedUV.push_back(uv2.x);
	m_mergedUV.push_back(uv2.y);
	m_mergedUV.push_back(uv1.x);
	m_mergedUV.push_back(uv1.y);
	
	// Left
	m_mergedMesh.push_back(point6.x);
	m_mergedMesh.push_back(point6.y);
	m_mergedMesh.push_back(point6.z);
	m_mergedMesh.push_back(point1.x);
	m_mergedMesh.push_back(point1.y);
	m_mergedMesh.push_back(point1.z);
	m_mergedMesh.push_back(point4.x);
	m_mergedMesh.push_back(point4.y);
	m_mergedMesh.push_back(point4.z);

	// p6 uv2, p1 uv4, p4 uv3, p7 uv1
	m_mergedUV.push_back(uv2.x);
	m_mergedUV.push_back(uv2.y);
	m_mergedUV.push_back(uv4.x);
	m_mergedUV.push_back(uv4.y);
	m_mergedUV.push_back(uv3.x);
	m_mergedUV.push_back(uv3.y);
	

	// Left
	m_mergedMesh.push_back(point6.x);
	m_mergedMesh.push_back(point6.y);
	m_mergedMesh.push_back(point6.z);
	m_mergedMesh.push_back(point4.x);
	m_mergedMesh.push_back(point4.y);
	m_mergedMesh.push_back(point4.z);
	m_mergedMesh.push_back(point7.x);
	m_mergedMesh.push_back(point7.y);
	m_mergedMesh.push_back(point7.z);

	// p6 uv2, p1 uv4, p4 uv3, p7 uv1
	m_mergedUV.push_back(uv2.x);
	m_mergedUV.push_back(uv2.y);
	m_mergedUV.push_back(uv3.x);
	m_mergedUV.push_back(uv3.y);
	m_mergedUV.push_back(uv1.x);
	m_mergedUV.push_back(uv1.y);
	

	// Top
	m_mergedMesh.push_back(point4.x);
	m_mergedMesh.push_back(point4.y);
	m_mergedMesh.push_back(point4.z);
	m_mergedMesh.push_back(point3.x);
	m_mergedMesh.push_back(point3.y);
	m_mergedMesh.push_back(point3.z);
	m_mergedMesh.push_back(point8.x);
	m_mergedMesh.push_back(point8.y);
	m_mergedMesh.push_back(point8.z);

	
	m_mergedUV.push_back(uv1.x);
	m_mergedUV.push_back(uv1.y);
	m_mergedUV.push_back(uv3.x);
	m_mergedUV.push_back(uv3.y);
	m_mergedUV.push_back(uv4.x);
	m_mergedUV.push_back(uv4.y);
	

	// Top
	m_mergedMesh.push_back(point4.x);
	m_mergedMesh.push_back(point4.y);
	m_mergedMesh.push_back(point4.z);
	m_mergedMesh.push_back(point8.x);
	m_mergedMesh.push_back(point8.y);
	m_mergedMesh.push_back(point8.z);
	m_mergedMesh.push_back(point7.x);
	m_mergedMesh.push_back(point7.y);
	m_mergedMesh.push_back(point7.z);

	
	m_mergedUV.push_back(uv1.x);
	m_mergedUV.push_back(uv1.y);
	m_mergedUV.push_back(uv4.x);
	m_mergedUV.push_back(uv4.y);
	m_mergedUV.push_back(uv2.x);
	m_mergedUV.push_back(uv2.y);
	

	// Bottom
	m_mergedMesh.push_back(point6.x);
	m_mergedMesh.push_back(point6.y);
	m_mergedMesh.push_back(point6.z);
	m_mergedMesh.push_back(point5.x);
	m_mergedMesh.push_back(point5.y);
	m_mergedMesh.push_back(point5.z);
	m_mergedMesh.push_back(point2.x);
	m_mergedMesh.push_back(point2.y);
	m_mergedMesh.push_back(point2.z);

	
	m_mergedUV.push_back(uv1.x);
	m_mergedUV.push_back(uv1.y);
	m_mergedUV.push_back(uv3.x);
	m_mergedUV.push_back(uv3.y);
	m_mergedUV.push_back(uv4.x);
	m_mergedUV.push_back(uv4.y);
	

	// Bottom
	m_mergedMesh.push_back(point6.x);
	m_mergedMesh.push_back(point6.y);
	m_mergedMesh.push_back(point6.z);
	m_mergedMesh.push_back(point2.x);
	m_mergedMesh.push_back(point2.y);
	m_mergedMesh.push_back(point2.z);
	m_mergedMesh.push_back(point1.x);
	m_mergedMesh.push_back(point1.y);
	m_mergedMesh.push_back(point1.z);

	
	m_mergedUV.push_back(uv1.x);
	m_mergedUV.push_back(uv1.y);
	m_mergedUV.push_back(uv4.x);
	m_mergedUV.push_back(uv4.y);
	m_mergedUV.push_back(uv2.x);
	m_mergedUV.push_back(uv2.y);
	
}

void VoxelMerger::createMesh(GLuint &vao, GLuint& indiceCount)
{
	if (m_mergedMesh.size() > 0) // Only try create a mesh if data actually exists!
	{
		// bind VAO
		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);

		bindVBO(&m_mergedMesh[0], 0, GL_ARRAY_BUFFER, GL_STREAM_DRAW, GL_FLOAT, GL_FALSE, sizeof(GLfloat)*m_mergedMesh.size(), 3);
		bindVBO(&m_mergedUV[0], 1, GL_ARRAY_BUFFER, GL_STREAM_DRAW, GL_FLOAT, GL_FALSE, sizeof(GLfloat)*m_mergedUV.size(), 2);

		indiceCount = m_mergedMesh.size() / 3;

		m_mergedMesh.clear();
		m_mergedUV.clear();
	}
}

GLuint VoxelMerger::bindVBO(const GLvoid* data, GLint attribIndex, GLenum targetType, GLenum drawMode, GLenum dataType, GLboolean normalized, size_t sizeOfData, GLuint numberOfDataPerValue)
{
	GLuint vbo;
	glGenBuffers(1, &vbo);
	glBindBuffer(targetType, vbo);
	glBufferData(targetType, sizeOfData, data, drawMode);

	// if it has a -ve attrib index then its not a normal attribute type, such as indice values that use GL_ELEMENT_ARRAY_BUFFER!
	if (attribIndex >= 0)
	{
		glEnableVertexAttribArray(attribIndex);
		glVertexAttribPointer(attribIndex, numberOfDataPerValue, dataType, normalized, 0, 0);
	}

	return vbo;
}