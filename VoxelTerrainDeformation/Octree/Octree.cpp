#include "Octree.h"
#include "InternalNode.h"
#include "ExternalNode.h"

#include <queue>
#include <iostream>

/*
	Generates an Octree from passed in data
	@param int dimensionOfTree - the dimension of the tree in cubes i.e 16x16x16 (each cube is 2 units big so technically it takes up a 32 unit range)
	we work out the depth from this
*/
Octree::Octree(int dimensionOfData, std::vector<std::vector<std::vector<std::vector<int>>>> densityField)
{
	/*
	std::vector<std::shared_ptr<ExternalNode>> nodeList;
	
	// k = int based co-ordinate, - dimensionOfData (to negate it into the octrees -16 to 16 dimensionallity
	// it should technically be the total voxel space width in unit size / 2 but since the dimensonOfData
	// is actually half the voxel space anyway when we use a unit space of 2 it's fine to just use
	// dimensionOfData, the + 1 is to move the co-ordinate to the center of the voxels space i.e a 1x1x1 cube 
	// sent to the bottom left of a cube wouldn't be centered at -16, -16, -16, but -15,-15,-15! (if it's unit size
	// is 1, which it is for this by default)
	for (int i = 0; i < densityField.size(); ++i)
		for (int j = 0; j < densityField[i].size(); ++j)
			for (int k = 0; k < densityField[i][j].size(); ++k)
				for (int i2 = 0; i2 < densityField[i][j][k].size(); ++i2)
				{
					if (checkSignChange(densityField[i][j][k]) == true)
					{
						nodeList.push_back(std::shared_ptr<ExternalNode>(new ExternalNode(densityField[i][j][k], glm::vec3((i * 2 - dimensionOfData) + 1, (j * 2 - dimensionOfData) + 1, (k * 2 - dimensionOfData) + 1), glm::vec3(1, 1, 1))));
					}
				}

	int i = 1;
	int depth = 0;
	
	while (i <= dimensionOfData){
		depth++; // depth the tree goes down until
		// multiply i by 2 as if we're partitioning 
		// each time the depth level changes i.e starts off at 1x1,
		// then 2x2 then 4x4 etc. till the dimensions been reached 
		i *= 2; 
	}
	
	glm::vec3 bottomLeftOftree(0 - dimensionOfData, 0 - dimensionOfData, 0 - dimensionOfData);
	glm::vec3 dimensionOfAboveLevel(nodeList[0]->getDimensions().x * 2, nodeList[0]->getDimensions().y * 2, nodeList[0]->getDimensions().z * 2);
	glm::vec3 currentTreePlacement = bottomLeftOftree + dimensionOfAboveLevel;
	while (i < depth){
		
		std::vector<std::shared_ptr<ExternalNode>> nextList;
		
		while (nodeList.size() > 0)
		{
			nextList.push_back(std::shared_ptr<ExternalNode>(new InternalNode(currentTreePlacement, dimensionOfAboveLevel)));
			int childCount = 0; // the current child we're on within the external node

			// test current level bounding data against upper level bounding data
			// to insert it
			for (int j = 0; j < nodeList.size(); j++)
			{
				if (childCount > 7) // child count is 7 return!
					break;

				// normal bbox collision need to change once done to use bounding box class/function
				if ((nodeList[j]->getPos().x < currentTreePlacement.x + dimensionOfAboveLevel.x
					&& nodeList[j]->getPos().x > currentTreePlacement.x - dimensionOfAboveLevel.x)
					&& (nodeList[j]->getPos().y < currentTreePlacement.y + dimensionOfAboveLevel.y
					&& nodeList[j]->getPos().y > currentTreePlacement.y - dimensionOfAboveLevel.y)
					&& (nodeList[j]->getPos().z < currentTreePlacement.z + dimensionOfAboveLevel.z
					&& nodeList[j]->getPos().z > currentTreePlacement.z - dimensionOfAboveLevel.z))
				{
					nextList[nextList.size() - 1]->getChildren()[childCount] = nodeList[j];
				}

				childCount++;
			}



		}

			// check if any voxels from the node list fall within this upper level bounding area
			// if it does, add it as a child to the level and remove it from the nodelist

		
		// once done put the new surface level into an upper queue 
	}
}

// Tests if there is a sign change across the voxels data, if there is then the
// surface runs through it and the function will return true
// sign data is indexed like so front:[0]---[2] back: [4]---[6]
//									   |     |         |     |
//									  [1]---[3]       [5]---[7]
// return param bool - true if there is a change i.e a surface runs through it, 
// false if there isn't
bool Octree::checkSignChange(std::vector<int> signData)
{
	if (signData.size() != 8) // too little sign data, incorrect format
		return false;

	if (signData[0] != signData[1])
		return true;

	if (signData[2] != signData[3])
		return true;

	if (signData[4] != signData[5])
		return true;

	if (signData[6] != signData[7])
		return true;

	return false;
	*/
}
