#pragma once
#include <GL\glew.h>
#include <memory>

#include "../Camera/CameraManager.h"
#include "../Shader Manager/ShaderManager.h"

class Voxel
{

public:
	Voxel(bool isVisible){ m_isVisible = isVisible; voxelCount += 1; }
	~Voxel(){}
	bool getVisible(){ return m_isVisible; }
	static int getVoxelCount(){ return voxelCount; }
	
private:
	// Per voxel variables
	bool m_isVisible;

	// Shared Variables for memory saving
	static int voxelCount; 
};