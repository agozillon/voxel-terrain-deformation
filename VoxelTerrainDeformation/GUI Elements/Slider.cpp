#include "Slider.h"

#include <glm/gtc/matrix_transform.hpp>
#include <iostream>

Slider::Slider(std::shared_ptr<TextToTexture> textToTexture, std::string name, std::shared_ptr<Texture2D> bgTexture, std::shared_ptr<Texture2D> sliderTexture, glm::vec2 pos, glm::vec2 bgScale, glm::vec2 sliderScale, float min, float max, std::string shader,
	GLuint meshVAO, GLuint meshIndexCount)
: m_textToTexture(textToTexture), m_name(name), m_bgTexture(bgTexture), m_sliderTexture(sliderTexture), m_pos(pos), m_bgScale(bgScale), m_sliderScale(sliderScale), m_min(min), m_max(max), m_currentPos(pos.x), m_shader(shader), m_meshVAO(meshVAO), m_meshIndexCount(meshIndexCount)
{
	// creating a bounding square for the slider piece that we move up and down the slider
	// the quads are 1x1 so multiplying it by the slider scale should get us the size of the slider piece 
	m_boundingSquare = std::shared_ptr<BoundingSquare>(new BoundingSquare(m_pos, glm::vec2(1.0, 1.0) * sliderScale));

	SDL_Color red = { 256, 0, 0 };
	m_sliderValue = calculateSliderValue();
	
	m_textTexture = m_textToTexture->createTextTexture((m_name + std::to_string(m_sliderValue)).c_str(), red);
	
}

void Slider::render(std::shared_ptr<ShaderManager> shaderManager, std::shared_ptr<CameraManager> cameraManager)
{
	glDepthMask(GL_FALSE);

	// rendering the slider text
	m_textTexture->bindTexture(GL_TEXTURE0);

	// model * view 
	glm::mat4 mv = glm::translate(mat4(1.0), glm::vec3(m_pos.x, m_pos.y + 1.75 * m_bgScale.y, -1.0f));
	mv = glm::scale(mv, glm::vec3(m_bgScale.x, m_bgScale.y, 1.0f));

	shaderManager->getShaderProgram("Textured Shader")->setUniformMatrix4fv("modelview", 1, false, glm::value_ptr(mv));
	shaderManager->getShaderProgram("Textured Shader")->setUniformMatrix4fv("projection", 1, false, glm::value_ptr(glm::mat4(1.0f)));
	
	glBindVertexArray(m_meshVAO); // bind mesh we'll use for all GUI sliders, should be a quad
	glDrawElements(GL_TRIANGLES, m_meshIndexCount, GL_UNSIGNED_INT, 0);
	
	// rendering the background that we move the slider along
	m_bgTexture->bindTexture(GL_TEXTURE0);

	// model * view 
	mv = glm::translate(glm::mat4(1.0), glm::vec3(m_pos.x, m_pos.y, -1.0f));
	mv = glm::scale(mv, glm::vec3(m_bgScale.x, m_bgScale.y, 1.0f));

	shaderManager->getShaderProgram("Textured Shader")->setUniformMatrix4fv("modelview", 1, false, glm::value_ptr(mv));
	shaderManager->getShaderProgram("Textured Shader")->setUniformMatrix4fv("projection", 1, false, glm::value_ptr(glm::mat4(1.0)));

	glDrawElements(GL_TRIANGLES, m_meshIndexCount, GL_UNSIGNED_INT, 0);
	
	// rendering the slider point we move around
	m_sliderTexture->bindTexture(GL_TEXTURE0);

	// model * view 
	mv = glm::translate(mat4(1.0), glm::vec3(m_currentPos, m_pos.y, -1.0f));
	mv = glm::scale(mv, glm::vec3(m_sliderScale.x, m_sliderScale.y, 1.0f));

	shaderManager->getShaderProgram("Textured Shader")->setUniformMatrix4fv("modelview", 1, false, glm::value_ptr(mv));
	shaderManager->getShaderProgram("Textured Shader")->setUniformMatrix4fv("projection", 1, false, glm::value_ptr(glm::mat4(1.0f)));

	glDrawElements(GL_TRIANGLES, m_meshIndexCount, GL_UNSIGNED_INT, 0);

	glDepthMask(GL_TRUE);
}

void Slider::moveSlider(float xPos)
{
	if (xPos < m_pos.x + 1.0 * m_bgScale.x && xPos > m_pos.x - 1.0 * m_bgScale.x)
	{
		m_currentPos = xPos;
		m_boundingSquare->updatePos(glm::vec2(m_currentPos, m_pos.y));
		m_sliderValue = calculateSliderValue();
		SDL_Color red = { 256, 0, 0 };
		m_textTexture = m_textToTexture->createTextTexture((m_name + std::to_string(m_sliderValue)).c_str(), red);
	}
}

// Function that passes back the value between min and max that corresponds to the position of the slider
// essentially just calculates the lerp percentage from the slider position on the background and then
// uses this value to lerp the return value between min and max 
// @return float - slider value at current point in relation to the sliders min and max
float Slider::calculateSliderValue()
{
	float lerpPercent = (m_currentPos - (m_pos.x - (1.0f * m_bgScale.x))) / ((m_pos.x + (1.0f * m_bgScale.x)) - (m_pos.x - (1.0f * m_bgScale.x)));
	return m_min + (m_max - m_min) * lerpPercent;
}
