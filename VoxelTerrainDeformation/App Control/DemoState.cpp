#include "DemoState.h"
#include "../Mouse/Mouse.h"
#include "../Output Texture Test/ppm.h"
#include "../Texture/TextToTexture.h"
#include "../Voxel Renderer/Voxel.h"


#include <cmath>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>
#include <SDL_keyboard.h>

#pragma region Constructors & Destructors 

// constructor
DemoState::DemoState()
{
	// initalize all relevant managers
	m_cameraManager = std::shared_ptr<CameraManager>(new CameraManager());
	m_shaderManager = std::shared_ptr<ShaderManager>(new ShaderManager());
	m_textureLoader = std::shared_ptr<TextureLoader>(new TextureLoader());
	m_assimpLoader = std::shared_ptr<AssimpLoader>(new AssimpLoader());
	m_perlinNoise = std::shared_ptr<PerlinNoise>(new PerlinNoise(237));
	
}

// destructor
DemoState::~DemoState()
{
}

#pragma endregion 

// function that is used to initilize all of the States variables and objects
void DemoState::init()
{
	// load cube
	m_assimpLoader->load("Resources/Models/cube.obj", m_cubeVAO, m_cubeIndexCount);
	
	GLuint tmpVAO;
	GLint tmpIndexCount;

	m_assimpLoader->load("Resources/Models/quad.obj", tmpVAO, tmpIndexCount);

	// load texture
	m_texture = m_textureLoader->loadTexture("Resources/Textures/cube.jpg");
	m_sliderTexture = m_textureLoader->loadTexture("Resources/Textures/slider.jpg");
	m_sliderBGTexture = m_textureLoader->loadTexture("Resources/Textures/sliderbg.png");

	// create TextToTexture object that we'll use to create the slider text
	std::shared_ptr<TextToTexture> sliderTextCreator = std::shared_ptr<TextToTexture>(new TextToTexture("Resources/Fonts/MavenPro-Regular.ttf", 10));
	
	// initialize our noise sliders
	numberOfSliders = 7;
	// 3D Frequency X Slider
	m_slider[0] = std::shared_ptr<Slider>(new Slider(sliderTextCreator, "Frequency X: ", m_sliderBGTexture, m_sliderTexture, glm::vec2(-0.75f, 0.925f), glm::vec2(0.2, 0.025), glm::vec2(0.05, 0.025), -10, 10, "Textured Shader", tmpVAO, tmpIndexCount));
	// 3D Frequency Y Slider
	m_slider[1] = std::shared_ptr<Slider>(new Slider(sliderTextCreator, "Frequency Y: ", m_sliderBGTexture, m_sliderTexture, glm::vec2(-0.75f, 0.825f), glm::vec2(0.2, 0.025), glm::vec2(0.05, 0.025), -10, 10, "Textured Shader", tmpVAO, tmpIndexCount));
	// 3D Frequency Z Slider
	m_slider[2] = std::shared_ptr<Slider>(new Slider(sliderTextCreator, "Frequency Z: ", m_sliderBGTexture, m_sliderTexture, glm::vec2(-0.75f, 0.725f), glm::vec2(0.2, 0.025), glm::vec2(0.05, 0.025), -10, 10, "Textured Shader", tmpVAO, tmpIndexCount));
	// Amplitude Slider 
	m_slider[3] = std::shared_ptr<Slider>(new Slider(sliderTextCreator, "Amplitude: ", m_sliderBGTexture, m_sliderTexture, glm::vec2(-0.75f, 0.625f), glm::vec2(0.2, 0.025), glm::vec2(0.05, 0.025), -10, 10, "Textured Shader", tmpVAO, tmpIndexCount));
	// Lacunarity Slider
	m_slider[4] = std::shared_ptr<Slider>(new Slider(sliderTextCreator, "Lacunarity: ", m_sliderBGTexture, m_sliderTexture, glm::vec2(-0.75f, 0.525f), glm::vec2(0.2, 0.025), glm::vec2(0.05, 0.025), -2, 2, "Textured Shader", tmpVAO, tmpIndexCount));
	// Persistence Slider
	m_slider[5] = std::shared_ptr<Slider>(new Slider(sliderTextCreator, "Persistence: ", m_sliderBGTexture, m_sliderTexture, glm::vec2(-0.75f, 0.425f), glm::vec2(0.2, 0.025), glm::vec2(0.05, 0.025), -2, 2, "Textured Shader", tmpVAO, tmpIndexCount));
	// Octave Slider
	m_slider[6] = std::shared_ptr<Slider>(new Slider(sliderTextCreator, "Octaves: ", m_sliderBGTexture, m_sliderTexture, glm::vec2(-0.75f, 0.325f), glm::vec2(0.2, 0.025), glm::vec2(0.05, 0.025), 1, 20, "Textured Shader", tmpVAO, tmpIndexCount));
	
	// load basic 3D texture shader Vert + Frag and then compile each + Compile into shader program 
	m_shaderManager->compileShader("Textured Shader Vert", "Resources/Shaders/Vertex Shaders/textured.vert", VERTEX);
	m_shaderManager->compileShader("Textured Shader Frag", "Resources/Shaders/Fragment Shaders/textured.frag", FRAGMENT);
	
	m_shaderManager->createShaderProgram("Textured Shader");
	m_shaderManager->addShaderProgramAttribute("Textured Shader", 0, "in_Position");
	m_shaderManager->addShaderProgramAttribute("Textured Shader", 1, "in_TexCoord");
	m_shaderManager->addShaderFragOutput("Textured Shader", 0, "out_Color");
	m_shaderManager->attachShaderToProgram("Textured Shader", "Textured Shader Vert");
	m_shaderManager->attachShaderToProgram("Textured Shader", "Textured Shader Frag");
	m_shaderManager->compileShaderProgram("Textured Shader");
	
	m_shaderManager->deleteShaders();

	m_shaderManager->getShaderProgram("Textured Shader")->use();

	// 4096 Density values * 8 for each sign value on each edge
	std::vector<std::vector<std::vector<std::vector<int>>>> densityField;
	

	densityField.resize(16);
	int total = 0;
	for (int i = 0; i < densityField.size(); ++i)
	{
		densityField[i].resize(16);
		for (int j = 0; j < densityField[i].size(); ++j)
		{
			densityField[i][j].resize(16);
			for (int k = 0; k < densityField[i][j].size(); ++k)
			{
				densityField[i][j][k].resize(8);
			}
		}
	}

	for (int i = 0; i < densityField.size(); ++i)
		for (int j = 0; j < densityField[i].size(); ++j)
			for (int k = 0; k < densityField[i][j].size(); ++k)
				for (int i2 = 0; i2 < densityField[i][j][k].size(); ++i2)
				{
					if (-0.4 + m_perlinNoise->fractionalBrownianMotion(glm::vec3(i + (i2 * 0.03), j + (i2 * 0.03), k + (i2 * 0.03)), glm::vec3(10.0f, 1.0f, 10.0f), 1.0f, 2.0f, 0.65f, 1, false) < 0)
						densityField[i][j][k][i2] = 1; // ground
				}

		

	m_octree = std::shared_ptr<Octree>(new Octree(16, densityField));



	/*
	ppm image(600, 450);

	unsigned int test = 0; 

	for (int j = 0; j < 450; ++j)
	{
		for (int i = 0; i < 600; ++i)
		{
			double x = (double)i / ((double)600.0f);
			double y = (double)j / ((double)450.0f);
			
		
			double n = m_perlinNoise->fractionalBrownianMotion(glm::vec3(x, y, 0.8f), glm::vec3(10.0f, 10.0f, 1.0f), 1.0f, 2.0f, 0.65f, 13, true);
		
	//		double n = m_perlinNoise->perlinNoise3D(glm::vec3(10 * x, 10 * y, 0.8), true);

		//    n = n - std::floor(n);
	//		n = abs(n);
			if (i == 381 && j == 145)
				std::cout << n * 255 << std::endl;
	
			image.r[test] = std::floor(255 * n);
			image.g[test] = std::floor(255 * n);
			image.b[test] = std::floor(255 * n);

			test++;
		}
	}

	image.write("tez.ppm");
	*/

	m_voxelManager = std::shared_ptr<VoxelChunkManager>(new VoxelChunkManager(m_cameraManager, m_shaderManager, m_texture, glm::vec3(32, 16, 32), glm::vec3(10.0f, 1.0f, 10.0f), 1.0f, 2.0f, 0.65f, 1));
	
	std::cout << "Number of Voxels: " << Voxel::getVoxelCount() << std::endl;

	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDepthMask(GL_TRUE);
}

// function that is used to set up certain things on entry like a reset function
void DemoState::enter()
{

}

// function that draws objects to screen
void DemoState::draw()
{
	glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	m_voxelManager->renderWorld();

	for (int i = 0; i < numberOfSliders; i++)
	{
		m_slider[i]->render(m_shaderManager, m_cameraManager);
	}
}

// function that is used to update the state (anything that happens while the state is occuring should go in here)
void DemoState::update()
{
	const Uint8 *keys = SDL_GetKeyboardState(NULL);

	for (int i = 0; i < numberOfSliders; i++)
	{
		if (m_slider[i]->getBoundingSquare()->detectPointCollision(glm::vec2(Mouse::getInstance()->getNDCX(), Mouse::getInstance()->getNDCY())) 
			&& Mouse::getInstance()->getLeftState() == MouseButtonState::BUTTON_STATE_DOWN)
		{
			m_slider[i]->moveSlider(Mouse::getInstance()->getNDCX());
		}
	}

	// if the enter key is pressed re-create the world with the slider variables
	if (keys[SDL_SCANCODE_RETURN])
	{
		m_voxelManager->createWorld(glm::vec3(m_slider[0]->getSliderValue(), m_slider[1]->getSliderValue(), m_slider[2]->getSliderValue()), m_slider[3]->getSliderValue(), m_slider[4]->getSliderValue(), m_slider[5]->getSliderValue(), std::round(m_slider[6]->getSliderValue()));
	}
		
	// Forward
	if ( keys[SDL_SCANCODE_W] ) m_cameraManager->move(0, 0, 0.1f);
	
	// Left
	if ( keys[SDL_SCANCODE_A] ) m_cameraManager->move(-0.1f, 0, 0);
	
	// Back
	if ( keys[SDL_SCANCODE_S] ) m_cameraManager->move(0, 0, -0.1f);
	
	// Right
	if ( keys[SDL_SCANCODE_D] ) m_cameraManager->move(0.1f, 0, 0);
	
	// Up
	if ( keys[SDL_SCANCODE_R] ) m_cameraManager->move(0, 0.1f, 0);
	
	// Down
	if ( keys[SDL_SCANCODE_F] ) m_cameraManager->move(0, -0.1f, 0);

	// rotate up
	if ( keys[SDL_SCANCODE_UP] ) m_cameraManager->updatePitch(0.5f);
	
    // rotate down
	if ( keys[SDL_SCANCODE_DOWN] ) m_cameraManager->updatePitch(-0.5f);

	// rotate right
	if ( keys[SDL_SCANCODE_RIGHT] || keys[SDL_SCANCODE_PERIOD] ) m_cameraManager->updateYaw(0.5f);
	
	// rotate left
	if (keys[SDL_SCANCODE_LEFT] || keys[SDL_SCANCODE_COMMA]) m_cameraManager->updateYaw(-0.5f);
}

