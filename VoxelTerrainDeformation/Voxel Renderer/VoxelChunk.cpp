#include "VoxelChunk.h"

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <iostream>

int VoxelChunk::m_chunkSize = 16;

VoxelChunk::VoxelChunk(std::shared_ptr<VoxelMerger> voxelMerger, int worldXPlacement, int worldYPlacement, int worldZPlacement, std::vector<std::vector<float>>& noiseValues)
{
	for (int i = 0; i < m_chunkSize; ++i)
	{
		for (int j = m_chunkSize; j > 0; --j)
		{
			for (int k = 0; k < m_chunkSize; ++k)
			{	
				if (noiseValues[(worldXPlacement * m_chunkSize) + i][(worldZPlacement * m_chunkSize) + k] > 0)
				{
					m_voxels[i][j-1][k] = std::shared_ptr<Voxel>(new Voxel(false));
					noiseValues[(worldXPlacement * m_chunkSize) + i][(worldZPlacement * m_chunkSize) + k] -= 1;
				}
				else
				{
					m_voxels[i][j-1][k] = std::shared_ptr<Voxel>(new Voxel(true));
				}

				// Add voxel to the chunk mesh, we don't render single Voxel Cubes as that
				// clogs up the GPU pipeline and means 4890(size of a 16x16x16 chunk) voxels
				// runs at 2 FPS. 
				if (m_voxels[i][j-1][k]->getVisible() == true)
				{
					voxelMerger->addVoxel(glm::vec3(((worldXPlacement * m_chunkSize) * 2) + (i * 2),
						((worldYPlacement * m_chunkSize) * 2) + ((j-1) * 2), ((-worldZPlacement * m_chunkSize) * 2) + (-k * 2)), 1);
				}
			}
		}
	}

	// Creates the mesh out of all the voxels we've been adding to the voxelMerger via the add Voxel function
	voxelMerger->createMesh(m_meshVAO, m_chunkIndexCount);
}

/*
	Render this Voxel Chunk
	@Param Shared Pointer ShaderManager shaderManager - pointer to the currently active shader manager
	@Param Shared Pointer CameraManager cameraManager - pointer to the currently active camera manager 
*/
void VoxelChunk::render(std::shared_ptr<CameraManager> cameraManager, std::shared_ptr<ShaderManager> shaderManager)
{
	// model * view 
	glm::mat4 mv = cameraManager->getView() * mat4(1.0);

	shaderManager->getShaderProgram("Textured Shader")->setUniformMatrix4fv("modelview", 1, false, glm::value_ptr(mv));
	shaderManager->getShaderProgram("Textured Shader")->setUniformMatrix4fv("projection", 1, false, glm::value_ptr(cameraManager->getProjection()));

	glBindVertexArray(m_meshVAO);
	glDrawArrays(GL_TRIANGLES, 0, m_chunkIndexCount);
}