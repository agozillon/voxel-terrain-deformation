#pragma once
#include <memory>
#include <vector>

#include "../Camera/CameraManager.h"
#include "../Noise Generators/PerlinNoise.h"
#include "../Shader Manager/ShaderManager.h"
#include "../Texture/Texture2D.h"

#include "VoxelChunk.h"
#include "VoxelMerger.h"

class VoxelChunkManager
{
public:
	VoxelChunkManager(std::shared_ptr<CameraManager> cameraManager, std::shared_ptr<ShaderManager> shaderManager, std::shared_ptr<Texture2D> voxelTexture,
		glm::vec3 worldSize, glm::vec3 frequency, float amplitude, float lacunarity, float persistence, int octaves);
	~VoxelChunkManager(){}
	void renderWorld();
	void createWorld(glm::vec3 frequency, float amplitude, float lacunarity, float persistence, int octaves);

private:	
	
	std::vector<std::shared_ptr<VoxelChunk>> m_world;
	std::shared_ptr<VoxelMerger> m_voxelMerger; // Essentially merged all the individual cube voxels into one mesh
	std::shared_ptr<CameraManager> m_cameraManager; // keeps pointers to the active managers so they don't have to constantly be passed in via the render call
	std::shared_ptr<ShaderManager> m_shaderManager;
	std::shared_ptr<Texture2D> m_voxelTexture; // I currently only use one texture for every voxel

	std::unique_ptr<PerlinNoise> m_perlinGenerator;
	glm::vec3 m_numberOfChunks;
};