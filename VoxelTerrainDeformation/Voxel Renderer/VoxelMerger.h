#pragma once

#include <GL/glew.h>
#include <glm/gtc/type_ptr.hpp>
#include <vector>

class VoxelMerger{
public:
	VoxelMerger(){}
	~VoxelMerger(){}
	void createMesh(GLuint& vao, GLuint& indiceCount);
	void addVoxel(glm::vec3 position, float blockSize);
	

private:
	GLuint bindVBO(const GLvoid* data, GLint attribIndex, GLenum targetType, GLenum drawMode, GLenum dataType, GLboolean normalized, size_t sizeOfData, GLuint numberOfDataPerValue);
	std::vector<float> m_mergedMesh;
	std::vector<float> m_mergedUV;
	std::vector<float> m_mergedNormals;
};