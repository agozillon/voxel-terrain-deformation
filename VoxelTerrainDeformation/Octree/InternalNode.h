#pragma once
#include "Node.h"
#include <memory>


class InternalNode : public Node
{
public:
	InternalNode(glm::vec3 pos, glm::vec3 dimensions){ m_pos = pos; m_dimensions = dimensions; }
	~InternalNode(){}
	std::shared_ptr<Node> getChild(int index){ return m_children[index]; }
	std::shared_ptr<Node>* getChildren(){ return m_children; }
	glm::vec3 getPos(){ return m_pos; }
	glm::vec3 getDimensions(){ return m_dimensions; }

private:
	std::shared_ptr<Node> m_children[8]; // nodes children
	glm::vec3 m_pos, m_dimensions;  // position of node and dimensions
};