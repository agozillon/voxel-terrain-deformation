#include "BoundingSquare.h"
#include <iostream>

// Takes an input point and checks if it's inside the bounding square
//@param vec2 collisionPos - the point we're checking for intersection 
//@return bool - returns true if a collision has occured, false if it hasn't
bool BoundingSquare::detectPointCollision(glm::vec2 collisionPos)
{
	// if the collision point is greater than the 
	// furthest point on the square then return false, no collision
	if (collisionPos.x > m_pos.x + m_dimension.x)
		return false;

	// if the collision point is smaller than the 
	// furthest left point on the square then return false, no collision
	if (collisionPos.x < m_pos.x - m_dimension.x)
		return false;

	// if the collision point is greater than the 
	// largest point on the square then return false, no collision
	if (collisionPos.y > m_pos.y + m_dimension.y)
		return false;

	// if the collision point is smaller than the 
	// lowest point on the square then return false, no collision
	if (collisionPos.y < m_pos.y - m_dimension.y)
		return false;

	// if it gets through all the checks then a collision has happened
	return true;
}