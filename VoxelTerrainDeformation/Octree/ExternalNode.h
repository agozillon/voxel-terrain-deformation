#pragma once
#include "Node.h"
#include <glm/gtc/type_ptr.hpp>
#include <vector>

// External Nodes are the nodes that contain all of the data
// within the Dual Contouring Octree, Internal Nodes are never traversed
// over to create a surface and thus storing data in there is unneeded
class ExternalNode : public Node
{
public:
	ExternalNode(std::vector<int> sign, glm::vec3 pos, glm::vec3 dimensions);
	glm::vec3 getPos(){ return m_pos; }
	glm::vec3 getDimensions(){ return m_dimensions; }

private:
	std::vector<int> m_sign; // a sign per corner

	glm::vec3 m_pos, m_dimensions; // minimal vertex/position & dimensions of the node

	// Quadratic Error Function Variables
	glm::mat3 m_ata; // 3x3 matrix, that's rows are the Hermite Datas normls
	glm::vec3 m_atb; // column vector, normals * intersection point
	double m_btb;	   // scalar,  
};