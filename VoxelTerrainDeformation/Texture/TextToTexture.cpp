#include "TextToTexture.h"
#include <iostream>   // for console output

// constructor that takes in a TTF_Font name and opens it and assigns it to textFont,
// takes an int in for the size of font 
TextToTexture::TextToTexture(std::string fontName, int fontSize)
{	
	// Opening text font, having one open for each TextToTexture may
	// end up being a bit memory intensive, possible future optimization
	m_textFont = TTF_OpenFont(fontName.c_str(), fontSize);
}

// destructor for deleting instantiated objects and in this case closing 
// the textFont
TextToTexture::~TextToTexture()
{
	TTF_CloseFont(m_textFont);
}

// function that takes in a set of chars and 3 ints representing RGB and then creates a
// texture version of the text and returns it
std::shared_ptr<Texture2D> TextToTexture::createTextTexture(const char * str, SDL_Color colour)
{	
	if (m_textFont == NULL)
	{
		std::cout << "Failed to open font" << std::endl;
	}

	SDL_Color bg = { 0, 0, 0 };

	SDL_Surface *stringImage;
	stringImage = TTF_RenderText_Blended(m_textFont,str,colour);

	if (stringImage == NULL)
	{
		std::cout << "String surface not created." << std::endl;
	}

	GLuint w = stringImage->w;
	GLuint h = stringImage->h;
	GLuint colours = stringImage->format->BytesPerPixel;

	GLuint format, internalFormat;
	
	if (colours == 4)  // alpha
	{  
		if (stringImage->format->Rmask == 0x000000ff)
		{
			format = GL_RGBA;
		}
	    else
		{
		    format = GL_BGRA;
		}
	} 
	else // no alpha
	{             
		if (stringImage->format->Rmask == 0x000000ff)
		{
			format = GL_RGB;
		}
	    else
		{
		    format = GL_BGR;
		}
	}

	internalFormat = (colours == 4) ? GL_RGBA : GL_RGB;

	std::shared_ptr<Texture2D> tmpTexture = std::shared_ptr<Texture2D>(new Texture2D(GL_TEXTURE_2D, w, h, internalFormat, format, GL_UNSIGNED_BYTE, stringImage->pixels));
	tmpTexture->bindTexture(GL_TEXTURE_2D);
	tmpTexture->updateSWrap(GL_CLAMP);
	tmpTexture->updateTWrap(GL_CLAMP);
	tmpTexture->updateMinFilter(GL_LINEAR);
	tmpTexture->updateMagFilter(GL_LINEAR);

	// SDL surface was used to generate the texture but is no longer
	// required. Release it to free memory
	SDL_FreeSurface(stringImage);

	return tmpTexture;
}
