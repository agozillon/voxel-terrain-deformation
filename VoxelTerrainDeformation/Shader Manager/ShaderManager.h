#pragma once

#include <map>
#include <string>
#include <memory>
#include <GL/glew.h>

#include "GLSLShaderProgram.h"
#include "GLSLShader.h"

// shader manager creates and contains a map of GLSL shaders and shader programs that we can access by name
// and modify
// USED IN EXTERNAL PROJECT
class ShaderManager{

public:
	ShaderManager(); // blank inline constructor
	~ShaderManager(){} // blank inline destructor 
	void compileShader(const char* shaderMapName, const char* fileName, ShaderType type); // compiles a shader from the passed in file name, frag/vert/geometry 
	
	void addShaderProgramAttribute(const char* programName, int index, const char* attribName); // adds an attribute(input) to the specified shader programs vertex shader
	void addShaderFragOutput(const char* programName, int index,  const char* fragOutputName);  // adds an fragment output to the specified shader programs fragment shader
	void defaultProgramAttributesAndOutputs(const char* programName);                           // sets up the basic inputs and outputs for the specified shader program
	void attachShaderToProgram(const char* programName, const char* shaderName);				// attaches a shader to the specfied shader program
	void createShaderProgram(const char* programName);											// instantiates a new shader program
	void compileShaderProgram(const char* programName); // compiles the shader program specified!

	std::shared_ptr<GLSLShaderProgram> getShaderProgram(const char* programName);  // passes a pointer from the named shader back as a return value 
	void deleteShaders(); // doesn't delete the programs, just the shaders should be called after all programs have been created!

private:
	// maps for holding Shaders and ShaderPrograms, also a "null" shader program to avoid accidental
	// errors from users of the class
	std::map<std::string, std::shared_ptr<GLSLShaderProgram>> m_shaderProgramList; 
	std::map<std::string, std::unique_ptr<GLSLShader>> m_shaderList;
	std::shared_ptr<GLSLShaderProgram> m_nullProgram;

	void printError(GLuint id) const; // prints shader errors, private function

};
