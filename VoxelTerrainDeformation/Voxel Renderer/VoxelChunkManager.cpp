#include "VoxelChunkManager.h"
#include <iostream>


#pragma region "Constructors & Destructors"
/*
Constructor for the voxel world 




@param vec3 worldSize - height, width and length of the world in voxels in 1x1x1 cubes you wish the world to be
and a bunch of noise paremters and then procedurally generates the world. Should be a number divisable by 16(chunk size)

The Below are Generation Parameters: 
@param float frequency - increases the frequency of each octave of noise, gets increased by lacunarity after each octave of noise
frequency can be seen as the space between each noise hump, higher frequency more points.
@param float amplitude - increases the amplitude of each noise point for all octaves, increasing the regular value outputted
by the noise, it gets reduced each octave so noise(0.5) * amiplitude(0.25) = 0.25
@param float lacunarity - varies the frequency by multiplying this number against it after each octave has been added on
effectively making each level of noise more "zoomed out" or "zoomed in" or with more/less noise points
@param float persistence - varies the amplitude each octave same way as the frequency, essentially increases the noise troughs
in size or reduces depending on the value you put in

*/
VoxelChunkManager::VoxelChunkManager(std::shared_ptr<CameraManager> cameraManager, std::shared_ptr<ShaderManager> shaderManager, std::shared_ptr<Texture2D> voxelTexture,
	glm::vec3 worldSize, glm::vec3 frequency, float amplitude, float lacunarity, float persistence, int octaves)
	: m_cameraManager(cameraManager), m_shaderManager(shaderManager), m_voxelTexture(voxelTexture)
{
	// passed in world size divided by the chunk size to get the number of chunks we need
	m_numberOfChunks = glm::vec3(worldSize.x / VoxelChunk::getChunkSize(),
		worldSize.y / VoxelChunk::getChunkSize(), worldSize.z / VoxelChunk::getChunkSize());

	m_voxelMerger = std::shared_ptr<VoxelMerger>(new VoxelMerger());

	// instantiating the Perlin Noise generator and seeding it, saves recreating it each time we create a world
	m_perlinGenerator = std::unique_ptr<PerlinNoise>(new PerlinNoise(231.0f)); 

	// create the initial world via passed constructor values
	createWorld(frequency, amplitude, lacunarity, persistence, octaves); 
}

#pragma endregion

/*
	Simple function that creates a procedural world (of the size specified in the constructor) 
	from a Perlin Noise Generated Height Map, can be difficult to get good/nice looking terrain
*/
void VoxelChunkManager::createWorld(glm::vec3 frequency, float amplitude, float lacunarity, float persistence, int octaves)
{
	// Simple Perlin Heightmap Generation
	// For the number of grid values on the x and y
	int i, j, k;
	float width = m_numberOfChunks.x * VoxelChunk::getChunkSize();
	float height = m_numberOfChunks.y * VoxelChunk::getChunkSize();
	float length = m_numberOfChunks.z * VoxelChunk::getChunkSize();

	std::vector<std::vector<float>> noiseValues;

	for (i = 0; i < width; ++i)
	{
		std::vector<float> tmp;
		noiseValues.push_back(tmp);
		for (j = 0; j < length; ++j)
		{
			float noise = std::floor((height - 1) * m_perlinGenerator->fractionalBrownianMotion(glm::vec3(i / width, 0.8, j / length), frequency, amplitude, lacunarity, persistence, octaves, true));
		
			if (noise >= height)
				noise = height - 1;
			
			noiseValues[i].push_back(noise);
		}
	}

	m_world.clear(); // clear the old world
	
	for (i = 0; i < m_numberOfChunks.x; ++i) // x
	{
		for (j = 0; j < m_numberOfChunks.y; ++j) // y
		{
			for (k = 0; k < m_numberOfChunks.z; ++k) // z
			{
				// Basically we want to start from the top set of voxels if there is more than one
				// and not the bottom
				int y = 0;
				if (m_numberOfChunks.y > 1)
					y = m_numberOfChunks.y - (j + 1);
				else
					y = j;

				
				m_world.push_back(std::shared_ptr<VoxelChunk>(new VoxelChunk(m_voxelMerger, i, y, k, noiseValues)));
			}
		}
	}

}

/*
   Function that renders the Voxel World by calling all the voxel chunks render functions.
*/
void VoxelChunkManager::renderWorld()
{
	// Sets the shader to Textured Shader (doesn't support other shader types at the moment)
	// not hugelly difficult to extend to.
	m_shaderManager->getShaderProgram("Textured Shader")->use();

	// bind texture
	m_voxelTexture->bindTexture(GL_TEXTURE0);

	// loops through all the voxel chunks and calls there render function passing in the camera manager
	// and shader manager
	int currentChunk = 0;
	while (currentChunk < m_world.size())
	{
		m_world[currentChunk]->render(m_cameraManager, m_shaderManager);
		currentChunk += 1;
	}
}
