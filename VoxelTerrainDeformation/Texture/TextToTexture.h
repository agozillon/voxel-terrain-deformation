#pragma once

#include <GL\glew.h> // including glew to use GLuints
#include <memory>
#include <SDL_ttf.h> // including SDL true type font library to use ttf functions and types
#include <string>

#include "Texture2D.h"

// Texture class thats a slightly modified version of my RT3D Project class of a same name 
class TextToTexture 
{

public:
	TextToTexture(std::string fontName, int fontSize);				// constructor that takes in a true type font and sets it up
	~TextToTexture();												// destructor used to close the ttf_font in this case										
	std::shared_ptr<Texture2D> createTextTexture(const char * str, SDL_Color colour);   // function that creates a ttf_font texture from a passed in set of chars and 3 ints representing RGB and then returns it as a textureID
	
private:
	// text font pointer to hold the passed in font 
	TTF_Font* m_textFont;  
};

